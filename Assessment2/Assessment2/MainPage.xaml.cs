﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Assessment2
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        //MainPage rootPage = MainPage.
        public MainPage()
        {
            this.InitializeComponent();
            var priorities = new List<string>();
            priorities.Add("High");
            priorities.Add("Important");
            priorities.Add("Medium");

            var departments = new List<string>();
            departments.Add("Sales");
            departments.Add("Accounts");
            departments.Add("Development");
            departments.Add("Help");
            departments.Add("Maintenance");

            cboPriorities.ItemsSource = priorities;
            cboDepartment.ItemsSource = departments;

        }
       
        private async void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            if (txtFrm.Text != string.Empty && txtSub.Text != string.Empty && cboDepartment.SelectedValue != null)
            {
                string subjectTxt = "";
                if(cboPriorities.SelectedValue != null)
                {
                    subjectTxt = $"{cboPriorities.SelectedValue} - {txtSub.Text.Trim()}";
                }
                else
                {
                    subjectTxt = $"{txtSub.Text.Trim()}";
                }
                var previewDia = new MessageDialog($"From: {txtFrm.Text.Trim()}{Environment.NewLine}Subject: {subjectTxt}{Environment.NewLine}Message: {txtMsg.Text.Trim()}");
                previewDia.Commands.Add(new UICommand("Reset") { Id = 0 });
                previewDia.Commands.Add(new UICommand("Cancel") { Id = 1 });
                previewDia.Commands.Add(new UICommand($"Send To {cboDepartment.SelectedValue}") { Id = 2 });

                var result = await previewDia.ShowAsync();

                if ((int)result.Id == 0)
                {
                    ClearText();
                }
                else if ((int)result.Id == 2)
                {
                    if(txtMsg.Text.Trim() == string.Empty)
                    {
                        var warningDia = new MessageDialog($"You are about to send a empty email. Are you sure you want to continue?");
                        warningDia.Commands.Add(new UICommand("Yes") { Id = 0 });
                        warningDia.Commands.Add(new UICommand("Cancel") { Id = 1 });
                        var answer = await warningDia.ShowAsync();

                        if ((int)answer.Id == 1)
                        {
                            return;
                        }
                    }
                    var messageDia = new MessageDialog("Message sent");
                    await messageDia.ShowAsync();
                    ClearText();
                }
            }
            else
            {
                var errorDia = new MessageDialog("You must have a To, From and a Subject ");
                await errorDia.ShowAsync();
            }
        }

        private void ClearText()
        {
            txtMsg.Text = "";
            txtFrm.Text = "";
            cboDepartment.SelectedValue = "";
            txtSub.Text = "";
            cboPriorities.SelectedValue = "";
        }
    }
}
